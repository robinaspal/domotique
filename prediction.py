# -*- coding: utf-8 -*-
"""
Created on Tue Nov 17 11:19:57 2020

@author: robin
"""



import tensorflow as tf

import pandas as pd

import numpy as np

from matplotlib import pyplot as plt

ts = 'RDC-Séjour_CO2_GAS_CONCENTRATIONppm<>Avg'

# Split data

ds_train_1 = pd.read_csv('./conditionned_data/Day/2017-09-18.csv')

ds_train_2 = pd.read_csv('./conditionned_data/Day/2017-09-19.csv')

ds_train_3 = pd.read_csv('./conditionned_data/Day/2017-09-20.csv')
ds_train_4 = pd.read_csv('./conditionned_data/Day/2017-09-21.csv')
ds_train_5 = pd.read_csv('./conditionned_data/Day/2017-09-22.csv')

ds_train = pd.concat([ds_train_1, ds_train_2, ds_train_3, ds_train_4, ds_train_5], ignore_index=True)

ds_validation = pd.read_csv('./conditionned_data/Day/2017-09-23.csv')

ds_test = pd.read_csv('./conditionned_data/Day/2017-09-24.csv')

# Fix missing values

def fix_nan(data):

    for key in data:

        for i in range(0, len(data[key])):

            if (isinstance(data[key][i], float) and np.isnan(data[key][i])):

                if i ==0:

                    data.loc[(i,key)] = 0

                else:

                    data.loc[(i,key)] = data[key][i-1]

    return(data)

ds_train = fix_nan(ds_train)

ds_validation = fix_nan(ds_validation)

ds_test = fix_nan(ds_test)

train = ds_train[[ts, 'firstDate']]

test = ds_test[[ts, 'firstDate']]

validation = ds_validation[[ts, 'firstDate']]

 

def create_date(data):

    data['firstDateHour'] = pd.to_numeric(data['firstDate'].str[-8:-6])

    data['firstDateMinute'] = pd.to_numeric(data['firstDate'].str[-5:-3])

    data['date'] = (data['firstDateHour'] * 60 + data['firstDateMinute']) / 1440

    return data

create_date(train)

create_date(test)

create_date(validation)

# Normalize data

train_mean = train[ts].mean()

train_std = train[ts].std()

train[ts] = (train[ts] - train_mean) / train_std

test[ts] = (test[ts] - train_mean) / train_std

validation[ts] = (validation[ts] - train_mean) / train_std

# Create samples

TIME_PERIOD_INPUT = 30

TIME_PERIOD_OUTPUT = 10

TIME_PERIOD = TIME_PERIOD_INPUT + TIME_PERIOD_OUTPUT

STEP_TIME = TIME_PERIOD // 2

def create_windows(data):

    data_input = []

    data_output = []

    for i in range(STEP_TIME, len(data)-STEP_TIME, 1):

        temp = (list(train[ts][i-STEP_TIME:i+STEP_TIME]), train['date'][i])

        data_input.append(temp[0][:TIME_PERIOD_INPUT] + [temp[1]])

        data_output.append(temp[0][TIME_PERIOD_INPUT:])

    return data_input, data_output

train_input, train_output = create_windows(train)

validation_input, validation_output = create_windows(validation)

test_input, test_output = create_windows(test)

# Build model

model = tf.keras.models.Sequential([

    tf.keras.layers.Dense(units=32, activation='relu', input_shape=(TIME_PERIOD_INPUT+1,)),

    tf.keras.layers.Dense(units=32, activation='relu'),
    
    tf.keras.layers.Dense(units=32, activation='relu'),
    
    tf.keras.layers.Dense(units=32, activation='relu'),

    tf.keras.layers.Dense(units=10)]

    )

model.compile(optimizer='adam', loss=tf.losses.MeanSquaredError(), metrics=[tf.metrics.MeanAbsoluteError()])

model.summary()

# Train

res = model.fit(train_input, train_output, epochs=30, validation_data=(validation_input, validation_output), shuffle=True)

# Metrics evolution

plt.plot(res.history['mean_absolute_error'], label='train')

plt.plot(res.history['val_mean_absolute_error'], label='validation')

plt.title('Model Mean Absolute Error')

plt.ylabel('MAE')

plt.xlabel('epoch')

plt.legend(loc='upper right')

plt.show()
plt.plot(res.history['loss'], label='train')

plt.plot(res.history['val_loss'], label='validation')

plt.title('Model Loss')

plt.ylabel('loss')

plt.xlabel('epoch')

plt.legend(loc='upper right')

plt.show()

# Evaluation

score = model.evaluate(test_input, test_output, verbose=1)

# Prediction

pred_output = model.predict(test_input)

# Plot random prediction

random_samples = np.random.randint(0, len(pred_output), 3)

for j in random_samples:

    plt.figure(figsize=(10, 5))

    plt.plot([i for i in range(TIME_PERIOD_INPUT)], test_input[j][:-1], color='dodgerblue')

    plt.plot([i for i in range(TIME_PERIOD_INPUT, TIME_PERIOD)], test_output[j], color='limegreen')

    plt.plot([i for i in range(TIME_PERIOD_INPUT, TIME_PERIOD)], pred_output[j], color='orange')

    plt.yticks(np.arange(-1, 5.2, 0.2))

    plt.show()

